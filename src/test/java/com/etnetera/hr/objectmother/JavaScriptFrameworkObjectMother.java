package com.etnetera.hr.objectmother;

import com.etnetera.hr.data.HypeLevel;
import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.JavaScriptFrameworkVersion;
import com.etnetera.hr.dto.FullJavaScriptFrameworkRequestDto;
import com.etnetera.hr.dto.FullJavaScriptFrameworkVersionRequestDto;
import com.etnetera.hr.dto.JavaScriptFrameworkResponseDto;
import com.etnetera.hr.dto.JavaScriptFrameworkVersionResponseDto;
import com.etnetera.hr.dto.PatchJavaScriptFrameworkRequestDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Set;

/**
 * Provides objects related to JavaScript framework.
 *
 * @author Adam Bucher
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JavaScriptFrameworkObjectMother {

    public static final Long ID = 1L;
    public static final String NAME = "Angulular";
    public static final HypeLevel HYPE_LEVEL = HypeLevel.MIDDLE;

    private static final Long VERSION_ID = 1L;
    private static final String VERSION = "1.2.3";
    private static final LocalDate DEPRECATION_DATE = LocalDate.of(2042, 1, 1);

    private static final String NEW_NAME = "Rereact";
    private static final HypeLevel NEW_HYPE_LEVEL = HypeLevel.HIGH;

    public static JavaScriptFramework javaScriptFramework() {
        return new JavaScriptFramework()
                .setId(ID)
                .setName(NAME)
                .setVersions(Set.of(javaScriptFrameworkVersion()))
                .setHypeLevel(HYPE_LEVEL);
    }

    public static JavaScriptFrameworkVersion javaScriptFrameworkVersion() {
        return new JavaScriptFrameworkVersion()
                .setId(VERSION_ID)
                .setVersion(VERSION)
                .setDeprecationDate(DEPRECATION_DATE);
    }

    public static JavaScriptFrameworkResponseDto javaScriptFrameworkResponseDto() {
        return new JavaScriptFrameworkResponseDto()
                .setId(ID)
                .setName(NAME)
                .setVersions(Set.of(javaScriptFrameworkVersionResponseDto()))
                .setHypeLevel(HYPE_LEVEL);
    }

    public static JavaScriptFrameworkVersionResponseDto javaScriptFrameworkVersionResponseDto() {
        return new JavaScriptFrameworkVersionResponseDto()
                .setId(VERSION_ID)
                .setVersion(VERSION)
                .setDeprecationDate(DEPRECATION_DATE);
    }

    public static FullJavaScriptFrameworkRequestDto fullJavaScriptFrameworkRequestDto() {
        return new FullJavaScriptFrameworkRequestDto()
                .setName(NAME)
                .setVersions(Set.of(fullJavaScriptFrameworkVersionRequestDto()))
                .setHypeLevel(HYPE_LEVEL);
    }

    public static FullJavaScriptFrameworkVersionRequestDto fullJavaScriptFrameworkVersionRequestDto() {
        return new FullJavaScriptFrameworkVersionRequestDto()
                .setVersion(VERSION)
                .setDeprecationDate(DEPRECATION_DATE);
    }

    public static PatchJavaScriptFrameworkRequestDto patchJavaScriptFrameworkRequestDto() {
        return new PatchJavaScriptFrameworkRequestDto()
                .setName(NEW_NAME)
                .setHypeLevel(NEW_HYPE_LEVEL);
    }
}
