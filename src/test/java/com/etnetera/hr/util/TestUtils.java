package com.etnetera.hr.util;

import lombok.experimental.UtilityClass;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.ResourceUtils;

import java.io.IOException;

/**
 * Contains utility methods for testing.
 *
 * @author Adam Bucher
 */
@UtilityClass
public class TestUtils {

    /**
     * Returns JSON read from JSON file inside of resources/json directory.
     *
     * @param filename name of JSON file
     * @return JSON as byte array
     */
    public byte[] getResourceJson(final String filename) {
        try {
            final var jsonResource = ResourceUtils.getFile("classpath:json/" + filename);
            return FileCopyUtils.copyToByteArray(jsonResource);
        } catch (final IOException exception) {
            throw new RuntimeException(exception);
        }
    }
}
