package com.etnetera.hr.controller;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.dto.FullJavaScriptFrameworkRequestDto;
import com.etnetera.hr.dto.FullJavaScriptFrameworkVersionRequestDto;
import com.etnetera.hr.mapper.JavaScriptFrameworkMapper;
import com.etnetera.hr.objectmother.JavaScriptFrameworkObjectMother;
import com.etnetera.hr.service.JavaScriptFrameworkService;
import com.etnetera.hr.util.TestUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link JavaScriptFrameworkController}.
 *
 * @author Adam Bucher
 */
@WebMvcTest(JavaScriptFrameworkController.class)
class JavaScriptFrameworkControllerTest {

    private static final String FRAMEWORKS_RESOURCE = "/frameworks";
    private static final String FRAMEWORKS_BY_ID_RESOURCE = FRAMEWORKS_RESOURCE + "/{id}";
    private static final String FRAMEWORKS_VERSIONS_RESOURCE = FRAMEWORKS_BY_ID_RESOURCE + "/versions";
    private static final String FRAMEWORKS_VERSIONS_BY_ID_RESOURCE = FRAMEWORKS_VERSIONS_RESOURCE + "/{id}";

    private static final int STRING_MAX_LENGTH = 30;

    private static final Long NONEXISTENT_ID = -1L;

    @MockBean
    private JavaScriptFrameworkService service;

    @MockBean
    private JavaScriptFrameworkMapper mapper;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void givenValidRequestBody_whenCreateFramework_thenReturn200OkWithCreatedFramework() throws Exception {
        // given
        final var javaScriptFrameworkToCreate = JavaScriptFrameworkObjectMother.javaScriptFramework().setId(null);
        when(mapper.mapToJavaScriptFramework(JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkRequestDto()))
                .thenReturn(javaScriptFrameworkToCreate);

        final var createdJavaScriptFramework = JavaScriptFrameworkObjectMother.javaScriptFramework();
        when(service.createJavaScriptFramework(javaScriptFrameworkToCreate)).thenReturn(createdJavaScriptFramework);

        when(mapper.mapToJavaScriptFrameworkResponseDto(createdJavaScriptFramework))
                .thenReturn(JavaScriptFrameworkObjectMother.javaScriptFrameworkResponseDto());

        final var request = TestUtils.getResourceJson("fullJavaScriptFrameworkRequest.json");
        final var expectedResponseBody = TestUtils.getResourceJson("fullJavaScriptFrameworkResponse.json");

        // when, then
        mvc
                .perform(
                        MockMvcRequestBuilders.post(FRAMEWORKS_RESOURCE)
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json(new String(expectedResponseBody)));
    }

    @Test
    void givenMissingRequestBody_whenCreateFramework_thenReturn400BadRequest() throws Exception {
        mvc
                .perform(MockMvcRequestBuilders.post(FRAMEWORKS_RESOURCE))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @ParameterizedTest
    @MethodSource("invalidJavaScriptFrameworkRequestDto")
    void givenInvalidRequestBody_whenCreateFramework_thenReturn400BadRequest(
            final FullJavaScriptFrameworkRequestDto requestDto
    ) throws Exception {
        mvc
                .perform(
                        MockMvcRequestBuilders.post(FRAMEWORKS_RESOURCE)
                                .content(objectMapper.writeValueAsString(requestDto))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void givenExistingResourceId_whenFindFramework_thenReturn200OkWithFoundFramework() throws Exception {
        // given
        final var existingJavaScriptFramework = JavaScriptFrameworkObjectMother.javaScriptFramework();
        when(service.findJavaScriptFramework(JavaScriptFrameworkObjectMother.ID))
                .thenReturn(Optional.of(existingJavaScriptFramework));

        when(mapper.mapToJavaScriptFrameworkResponseDto(existingJavaScriptFramework))
                .thenReturn(JavaScriptFrameworkObjectMother.javaScriptFrameworkResponseDto());

        final var expectedResponseBody = TestUtils.getResourceJson("fullJavaScriptFrameworkResponse.json");

        // when, then
        mvc
                .perform(MockMvcRequestBuilders.get(FRAMEWORKS_BY_ID_RESOURCE, JavaScriptFrameworkObjectMother.ID))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(new String(expectedResponseBody)));
    }

    @Test
    void givenNonexistentResourceId_whenFindFramework_thenReturn404NotFound() throws Exception {
        // given
        when(service.findJavaScriptFramework(JavaScriptFrameworkObjectMother.ID)).thenReturn(Optional.empty());

        // when, then
        mvc
                .perform(MockMvcRequestBuilders.get(FRAMEWORKS_BY_ID_RESOURCE, NONEXISTENT_ID))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void givenQueryParameterName_whenSearchFrameworks_thenReturn200OkWithFoundFrameworks() throws Exception {
        // given
        final var foundJavaScriptFrameworks = Set.of(JavaScriptFrameworkObjectMother.javaScriptFramework());
        when(service.findJavaScriptFrameworks(JavaScriptFrameworkObjectMother.NAME, null))
                .thenReturn(foundJavaScriptFrameworks);

        when(mapper.mapToJavaScriptFrameworkResponseDtos(foundJavaScriptFrameworks))
                .thenReturn(Set.of(JavaScriptFrameworkObjectMother.javaScriptFrameworkResponseDto()));

        final var expectedResponseBody = TestUtils.getResourceJson("fullJavaScriptFrameworkResponse.json");

        // when, then
        mvc
                .perform(
                        MockMvcRequestBuilders.get(FRAMEWORKS_RESOURCE)
                                .param("name", JavaScriptFrameworkObjectMother.NAME)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("[" + new String(expectedResponseBody) + "]"));
    }

    @Test
    void givenQueryParameterHypeLevel_whenSearchFrameworks_thenReturn200OkWithFoundFrameworks() throws Exception {
        // given
        final var foundJavaScriptFrameworks = Set.of(JavaScriptFrameworkObjectMother.javaScriptFramework());
        when(service.findJavaScriptFrameworks(null, JavaScriptFrameworkObjectMother.HYPE_LEVEL))
                .thenReturn(foundJavaScriptFrameworks);

        when(mapper.mapToJavaScriptFrameworkResponseDtos(foundJavaScriptFrameworks))
                .thenReturn(Set.of(JavaScriptFrameworkObjectMother.javaScriptFrameworkResponseDto()));

        final var expectedResponseBody = TestUtils.getResourceJson("fullJavaScriptFrameworkResponse.json");

        // when, then
        mvc
                .perform(
                        MockMvcRequestBuilders.get(FRAMEWORKS_RESOURCE)
                                .param("hypeLevel", JavaScriptFrameworkObjectMother.HYPE_LEVEL.toString())
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("[" + new String(expectedResponseBody) + "]"));
    }

    @Test
    void givenQueryParameterNameAndHypeLevel_whenSearchFrameworks_thenReturn200OkWithFoundFrameworks()
            throws Exception {
        // given
        final var foundJavaScriptFrameworks = Set.of(JavaScriptFrameworkObjectMother.javaScriptFramework());
        when(
                service.findJavaScriptFrameworks(
                        JavaScriptFrameworkObjectMother.NAME, JavaScriptFrameworkObjectMother.HYPE_LEVEL
                )
        )
                .thenReturn(foundJavaScriptFrameworks);

        when(mapper.mapToJavaScriptFrameworkResponseDtos(foundJavaScriptFrameworks))
                .thenReturn(Set.of(JavaScriptFrameworkObjectMother.javaScriptFrameworkResponseDto()));

        final var expectedResponseBody = TestUtils.getResourceJson("fullJavaScriptFrameworkResponse.json");

        // when, then
        mvc
                .perform(
                        MockMvcRequestBuilders.get(FRAMEWORKS_RESOURCE)
                                .param("name", JavaScriptFrameworkObjectMother.NAME)
                                .param("hypeLevel", JavaScriptFrameworkObjectMother.HYPE_LEVEL.toString())
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("[" + new String(expectedResponseBody) + "]"));
    }

    @Test
    void givenValidRequestBody_whenPatchFramework_thenReturn200OkWithPatchedFramework() throws Exception {
        // given
        final var existingJavaScriptFramework = JavaScriptFrameworkObjectMother.javaScriptFramework();
        existingJavaScriptFramework.setName("Old name");
        when(service.findJavaScriptFramework(JavaScriptFrameworkObjectMother.ID))
                .thenReturn(Optional.of(existingJavaScriptFramework));

        final var patchJavaScriptFrameworkRequestDto = JavaScriptFrameworkObjectMother
                .patchJavaScriptFrameworkRequestDto();
        final var javaScriptFrameworkPatchSource = JavaScriptFrameworkObjectMother.javaScriptFramework().setId(null);
        when(mapper.mapToJavaScriptFramework(patchJavaScriptFrameworkRequestDto))
                .thenReturn(javaScriptFrameworkPatchSource);

        final var updatedJavaScriptFramework = JavaScriptFrameworkObjectMother.javaScriptFramework();
        when(service.updateJavaScriptFramework(existingJavaScriptFramework)).thenReturn(updatedJavaScriptFramework);

        final var javaScriptFrameworkResponseDto = JavaScriptFrameworkObjectMother.javaScriptFrameworkResponseDto();
        javaScriptFrameworkResponseDto.setName(patchJavaScriptFrameworkRequestDto.getName());
        javaScriptFrameworkResponseDto.setHypeLevel(patchJavaScriptFrameworkRequestDto.getHypeLevel());
        when(mapper.mapToJavaScriptFrameworkResponseDto(updatedJavaScriptFramework))
                .thenReturn(javaScriptFrameworkResponseDto);

        final var request = TestUtils.getResourceJson("patchJavaScriptFrameworkRequest.json");
        final var expectedResponseBody = TestUtils.getResourceJson("patchJavaScriptFrameworkResponse.json");

        // when, then
        mvc
                .perform(
                        MockMvcRequestBuilders.patch(FRAMEWORKS_BY_ID_RESOURCE, JavaScriptFrameworkObjectMother.ID)
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(new String(expectedResponseBody)));

        final var decoratedJavaScriptFrameworkCaptor = ArgumentCaptor.forClass(JavaScriptFramework.class);
        verify(mapper).decorateJavaScriptFramework(
                decoratedJavaScriptFrameworkCaptor.capture(), eq(javaScriptFrameworkPatchSource)
        );
        verify(service).updateJavaScriptFramework(decoratedJavaScriptFrameworkCaptor.getValue());
    }

    @Test
    void givenMissingRequestBody_whenPatchFramework_thenReturn400BadRequest() throws Exception {
        mvc
                .perform(MockMvcRequestBuilders.patch(FRAMEWORKS_BY_ID_RESOURCE, JavaScriptFrameworkObjectMother.ID))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void givenNonexistentResourceId_whenPatchFramework_thenReturn404NotFound() throws Exception {
        // given
        final var request = TestUtils.getResourceJson("fullJavaScriptFrameworkRequest.json");

        // when, then
        mvc
                .perform(
                        MockMvcRequestBuilders.patch(FRAMEWORKS_BY_ID_RESOURCE, NONEXISTENT_ID)
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void whenDeleteFramework_thenReturn200Ok() throws Exception {
        // given
        final var existingJavaScriptFramework = JavaScriptFrameworkObjectMother.javaScriptFramework();
        when(service.findJavaScriptFramework(JavaScriptFrameworkObjectMother.ID))
                .thenReturn(Optional.of(existingJavaScriptFramework));

        // when, then
        mvc
                .perform(MockMvcRequestBuilders.delete(FRAMEWORKS_BY_ID_RESOURCE, JavaScriptFrameworkObjectMother.ID))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(service).deleteJavaScriptFramework(JavaScriptFrameworkObjectMother.ID);
    }

    @Test
    void givenNonexistentResourceId_whenDeleteFramework_thenReturn404NotFound() throws Exception {
        // when, then
        mvc
                .perform(MockMvcRequestBuilders.delete(FRAMEWORKS_BY_ID_RESOURCE, JavaScriptFrameworkObjectMother.ID))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void givenValidRequestBody_whenCreateFrameworkVersion_thenReturn200OkWithCreatedFrameworkVersion()
            throws Exception {
        // given
        when(service.findJavaScriptFramework(JavaScriptFrameworkObjectMother.ID))
                .thenReturn(Optional.of(JavaScriptFrameworkObjectMother.javaScriptFramework()));

        final var javaScriptFrameworkVersionToCreate = JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion();
        javaScriptFrameworkVersionToCreate.setId(null);
        when(mapper.mapToJavaScriptFrameworkVersion(
                JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkVersionRequestDto()
        ))
                .thenReturn(javaScriptFrameworkVersionToCreate);

        final var createdJavaScriptFrameworkVersion = JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion();
        when(service.createJavaScriptFrameworkVersion(javaScriptFrameworkVersionToCreate))
                .thenReturn(createdJavaScriptFrameworkVersion);

        when(mapper.mapToJavaScriptFrameworkVersionResponseDto(createdJavaScriptFrameworkVersion))
                .thenReturn(JavaScriptFrameworkObjectMother.javaScriptFrameworkVersionResponseDto());

        final var request = TestUtils.getResourceJson("fullJavaScriptFrameworkVersionRequest.json");
        final var expectedResponseBody = TestUtils.getResourceJson("fullJavaScriptFrameworkVersionResponse.json");

        // when, then
        mvc
                .perform(
                        MockMvcRequestBuilders.post(FRAMEWORKS_VERSIONS_RESOURCE, JavaScriptFrameworkObjectMother.ID)
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json(new String(expectedResponseBody)));
    }

    @Test
    void givenNonexistentResourceId_whenCreateFrameworkVersion_thenReturn404NotFound() throws Exception {
        // given
        final var request = TestUtils.getResourceJson("fullJavaScriptFrameworkVersionRequest.json");

        // when, then
        mvc
                .perform(
                        MockMvcRequestBuilders.post(FRAMEWORKS_VERSIONS_RESOURCE, NONEXISTENT_ID)
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void givenMissingRequestBody_whenCreateFrameworkVersion_thenReturn400BadRequest() throws Exception {
        mvc
                .perform(MockMvcRequestBuilders.post(FRAMEWORKS_VERSIONS_RESOURCE, JavaScriptFrameworkObjectMother.ID))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @ParameterizedTest
    @MethodSource("invalidJavaScriptFrameworkVersionRequestDto")
    void givenInvalidRequestBody_whenCreateFrameworkVersion_thenReturn400BadRequest(
            final FullJavaScriptFrameworkVersionRequestDto requestDto
    ) throws Exception {
        mvc
                .perform(
                        MockMvcRequestBuilders.post(FRAMEWORKS_VERSIONS_RESOURCE, JavaScriptFrameworkObjectMother.ID)
                                .content(objectMapper.writeValueAsString(requestDto))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void givenValidRequestBody_whenUpdateFrameworkVersion_thenReturn200OkWithUpdatedFrameworkVersion()
            throws Exception {
        // given
        final var existingJavaScriptFrameworkVersion = JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion();
        existingJavaScriptFrameworkVersion.setFramework(JavaScriptFrameworkObjectMother.javaScriptFramework());
        when(service.findJavaScriptFrameworkVersion(JavaScriptFrameworkObjectMother.ID))
                .thenReturn(Optional.of(existingJavaScriptFrameworkVersion));

        final var javaScriptFrameworkVersionToUpdate = JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion();
        when(mapper.mapToJavaScriptFrameworkVersion(
                JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkVersionRequestDto()
        ))
                .thenReturn(javaScriptFrameworkVersionToUpdate);

        final var updatedJavaScriptFrameworkVersion = JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion();
        when(service.updateJavaScriptFrameworkVersion(javaScriptFrameworkVersionToUpdate))
                .thenReturn(updatedJavaScriptFrameworkVersion);

        when(mapper.mapToJavaScriptFrameworkVersionResponseDto(updatedJavaScriptFrameworkVersion))
                .thenReturn(JavaScriptFrameworkObjectMother.javaScriptFrameworkVersionResponseDto());

        final var request = TestUtils.getResourceJson("fullJavaScriptFrameworkVersionRequest.json");
        final var expectedResponseBody = TestUtils.getResourceJson("fullJavaScriptFrameworkVersionResponse.json");

        // when, then
        mvc
                .perform(
                        MockMvcRequestBuilders
                                .put(
                                        FRAMEWORKS_VERSIONS_BY_ID_RESOURCE,
                                        JavaScriptFrameworkObjectMother.ID, JavaScriptFrameworkObjectMother.ID
                                )
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(new String(expectedResponseBody)));
    }

    @Test
    void givenNonexistentResourceId_whenUpdateFrameworkVersion_thenReturn404NotFound() throws Exception {
        // given
        final var request = TestUtils.getResourceJson("fullJavaScriptFrameworkVersionRequest.json");

        // when, then
        mvc
                .perform(
                        MockMvcRequestBuilders
                                .put(
                                        FRAMEWORKS_VERSIONS_BY_ID_RESOURCE,
                                        JavaScriptFrameworkObjectMother.ID, NONEXISTENT_ID
                                )
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void givenForeignFrameworkResourceId_whenUpdateFrameworkVersion_thenReturn404NotFound() throws Exception {
        // given
        final var foreignJavaScriptFramework = JavaScriptFrameworkObjectMother.javaScriptFramework();
        foreignJavaScriptFramework.setId(JavaScriptFrameworkObjectMother.ID + 1);
        final var javaScriptFrameworkVersion = JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion();
        javaScriptFrameworkVersion.setFramework(foreignJavaScriptFramework);
        when(service.findJavaScriptFrameworkVersion(JavaScriptFrameworkObjectMother.ID))
                .thenReturn(Optional.of(javaScriptFrameworkVersion));

        final var request = TestUtils.getResourceJson("fullJavaScriptFrameworkVersionRequest.json");

        // when, then
        mvc
                .perform(
                        MockMvcRequestBuilders
                                .put(
                                        FRAMEWORKS_VERSIONS_BY_ID_RESOURCE,
                                        JavaScriptFrameworkObjectMother.ID, JavaScriptFrameworkObjectMother.ID
                                )
                                .content(request)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void givenMissingRequestBody_whenUpdateFrameworkVersion_thenReturn400BadRequest() throws Exception {
        mvc
                .perform(MockMvcRequestBuilders.put(
                        FRAMEWORKS_VERSIONS_BY_ID_RESOURCE,
                        JavaScriptFrameworkObjectMother.ID, JavaScriptFrameworkObjectMother.ID
                ))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @ParameterizedTest
    @MethodSource("invalidJavaScriptFrameworkVersionRequestDto")
    void givenInvalidRequestBody_whenUpdateFrameworkVersion_thenReturn400BadRequest(
            final FullJavaScriptFrameworkVersionRequestDto requestDto
    ) throws Exception {
        mvc
                .perform(
                        MockMvcRequestBuilders
                                .put(
                                        FRAMEWORKS_VERSIONS_BY_ID_RESOURCE,
                                        JavaScriptFrameworkObjectMother.ID, JavaScriptFrameworkObjectMother.ID
                                )
                                .content(objectMapper.writeValueAsString(requestDto))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void whenDeleteFrameworkVersion_thenReturn200Ok() throws Exception {
        // given
        final var existingJavaScriptFrameworkVersion = JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion();
        existingJavaScriptFrameworkVersion.setFramework(JavaScriptFrameworkObjectMother.javaScriptFramework());
        when(service.findJavaScriptFrameworkVersion(JavaScriptFrameworkObjectMother.ID))
                .thenReturn(Optional.of(existingJavaScriptFrameworkVersion));

        // when, then
        mvc
                .perform(
                        MockMvcRequestBuilders
                                .delete(
                                        FRAMEWORKS_VERSIONS_BY_ID_RESOURCE,
                                        JavaScriptFrameworkObjectMother.ID, JavaScriptFrameworkObjectMother.ID
                                )
                )
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(service).deleteJavaScriptFrameworkVersion(JavaScriptFrameworkObjectMother.ID);
    }

    @Test
    void givenNonexistentResourceId_whenDeleteFrameworkVersion_thenReturn404NotFound() throws Exception {
        // when, then
        mvc
                .perform(
                        MockMvcRequestBuilders
                                .delete(
                                        FRAMEWORKS_VERSIONS_BY_ID_RESOURCE,
                                        JavaScriptFrameworkObjectMother.ID, NONEXISTENT_ID
                                )
                )
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void givenForeignFrameworkResourceId_whenDeleteFrameworkVersion_thenReturn404NotFound() throws Exception {
        // given
        final var foreignJavaScriptFramework = JavaScriptFrameworkObjectMother.javaScriptFramework();
        foreignJavaScriptFramework.setId(JavaScriptFrameworkObjectMother.ID + 1);
        final var javaScriptFrameworkVersion = JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion();
        javaScriptFrameworkVersion.setFramework(foreignJavaScriptFramework);
        when(service.findJavaScriptFrameworkVersion(JavaScriptFrameworkObjectMother.ID))
                .thenReturn(Optional.of(javaScriptFrameworkVersion));

        // when, then
        mvc
                .perform(
                        MockMvcRequestBuilders
                                .delete(
                                        FRAMEWORKS_VERSIONS_BY_ID_RESOURCE,
                                        JavaScriptFrameworkObjectMother.ID, JavaScriptFrameworkObjectMother.ID
                                )
                )
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    private static Stream<Arguments> invalidJavaScriptFrameworkRequestDto() {
        return Stream.of(
                // instances with invalid JavaScript framework (base)
                Arguments.of(JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkRequestDto().setName(null)),
                Arguments.of(JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkRequestDto().setName("")),
                Arguments.of(JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkRequestDto().setName(" ")),
                Arguments.of(
                        JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkRequestDto()
                                .setName("x".repeat(STRING_MAX_LENGTH + 1))
                ),
                Arguments.of(JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkRequestDto().setHypeLevel(null)),

                // instances with invalid JavaScript framework version
                Arguments.of(
                        JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkRequestDto()
                                .setVersions(Set.of(
                                        JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkVersionRequestDto()
                                                .setVersion(null)
                                ))
                ),
                Arguments.of(
                        JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkRequestDto()
                                .setVersions(Set.of(
                                        JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkVersionRequestDto()
                                                .setVersion("")
                                ))
                ),
                Arguments.of(
                        JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkRequestDto()
                                .setVersions(Set.of(
                                        JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkVersionRequestDto()
                                                .setVersion(" ")
                                ))
                ),
                Arguments.of(
                        JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkRequestDto()
                                .setVersions(Set.of(
                                        JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkVersionRequestDto()
                                                .setVersion("x".repeat(STRING_MAX_LENGTH + 1))
                                ))
                )
        );
    }

    private static Stream<Arguments> invalidJavaScriptFrameworkVersionRequestDto() {
        return Stream.of(
                Arguments.of(
                        JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkVersionRequestDto().setVersion(null)
                ),
                Arguments.of(
                        JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkVersionRequestDto().setVersion(" ")
                ),
                Arguments.of(
                        JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkVersionRequestDto()
                                .setVersion("x".repeat(STRING_MAX_LENGTH + 1))
                )
        );
    }
}
