package com.etnetera.hr.controller;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.dto.JavaScriptFrameworkResponseDto;
import com.etnetera.hr.dto.JavaScriptFrameworkVersionResponseDto;
import com.etnetera.hr.objectmother.JavaScriptFrameworkObjectMother;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;

import java.time.LocalDate;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

/**
 * Integration tests for {@link JavaScriptFrameworkController}.
 *
 * @author Adam Bucher
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class JavaScriptFrameworkControllerIT {

    private static final String HOST = "http://localhost";

    private static final String FRAMEWORKS_RESOURCE = "/frameworks";
    private static final String FRAMEWORKS_BY_ID_RESOURCE = FRAMEWORKS_RESOURCE + "/{id}";
    private static final String FRAMEWORKS_VERSIONS_RESOURCE = FRAMEWORKS_BY_ID_RESOURCE + "/versions";
    private static final String FRAMEWORKS_VERSIONS_BY_ID_RESOURCE = FRAMEWORKS_VERSIONS_RESOURCE + "/{id}";

    @LocalServerPort
    private int port;

    @Autowired
    private JavaScriptFrameworkRepository repository;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @Order(0)
    void givenEmptyRepository_whenFindFrameworks_thenReturnEmptyArray() {
        // given
        assumeThat(repository.count()).isZero();

        // when
        final var response = restTemplate.exchange(
                RequestEntity.get(resolveUrl(FRAMEWORKS_RESOURCE)).build(),
                new ParameterizedTypeReference<Iterable<JavaScriptFrameworkResponseDto>>() {}
        );

        // then
        assertSoftly(softly -> {
            softly.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

            final var javaScriptFrameworkResponseDtos = response.getBody();
            softly.assertThat(javaScriptFrameworkResponseDtos).isEmpty();
        });
    }

    @Test
    @Order(1)
    void whenCreateFramework_thenReturnCreatedFramework() {
        // given
        final var requestBody = JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkRequestDto();

        // when
        final var response = restTemplate.exchange(
                RequestEntity.post(resolveUrl(FRAMEWORKS_RESOURCE)).body(requestBody),
                JavaScriptFrameworkResponseDto.class
        );

        // then
        assertThat(response.getBody()).isNotNull();
        assertSoftly(softly -> {
            softly.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);

            final var javaScriptFrameworkResponseDto = response.getBody();
            // check that IDs were set and returned
            softly.assertThat(javaScriptFrameworkResponseDto.getId()).isNotNull();
            softly.assertThat(javaScriptFrameworkResponseDto.getVersions())
                    .allSatisfy(version -> assertThat(version.getId()).isNotNull());
            // check that rest of the properties were the same as in input
            softly.assertThat(javaScriptFrameworkResponseDto)
                    .usingRecursiveComparison()
                    .ignoringFields("id", "versions.id")
                    .isEqualTo(requestBody);
        });
    }

    @Test
    @Order(2)
    void whenFindFrameworks_thenReturnFoundFrameworks() {
        // when
        final var response = restTemplate.exchange(
                RequestEntity.get(resolveUrl(FRAMEWORKS_RESOURCE)).build(),
                new ParameterizedTypeReference<Iterable<JavaScriptFrameworkResponseDto>>() {}
        );

        // then
        assertThat(response.getBody()).isNotNull().hasSize(1);
        assertSoftly(softly -> {
            softly.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

            final var javaScriptFrameworkResponseDto = response.getBody().iterator().next();
            softly.assertThat(javaScriptFrameworkResponseDto.getVersions())
                    .allSatisfy(version -> assertThat(version.getId()).isNotNull());
            softly.assertThat(javaScriptFrameworkResponseDto)
                    .usingRecursiveComparison()
                    .ignoringFields("versions.id")
                    .isEqualTo(JavaScriptFrameworkObjectMother.javaScriptFramework());
        });
    }

    @Test
    @Order(2)
    void whenFindFramework_thenReturnFoundFramework() {
        // when
        final var response = restTemplate.exchange(
                RequestEntity.get(resolveUrl(FRAMEWORKS_BY_ID_RESOURCE), JavaScriptFrameworkObjectMother.ID).build(),
                JavaScriptFrameworkResponseDto.class
        );

        // then
        assertThat(response.getBody()).isNotNull();
        assertSoftly(softly -> {
            softly.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

            final var javaScriptFrameworkResponseDto = response.getBody();
            softly.assertThat(javaScriptFrameworkResponseDto.getVersions())
                    .allSatisfy(version -> assertThat(version.getId()).isNotNull());
            softly.assertThat(javaScriptFrameworkResponseDto)
                    .usingRecursiveComparison()
                    .ignoringFields("versions.id")
                    .isEqualTo(JavaScriptFrameworkObjectMother.javaScriptFramework());
        });
    }

    @ParameterizedTest
    @MethodSource("findFrameworksQueryParametersForPresentResource")
    @Order(2)
    void givenQueryParameters_whenFindFrameworks_thenReturnFoundFrameworks(final String queryParameters) {
        // when
        final var response = restTemplate.exchange(
                RequestEntity.get(resolveUrl(FRAMEWORKS_RESOURCE) + queryParameters).build(),
                new ParameterizedTypeReference<Iterable<JavaScriptFramework>>() {}
        );

        // then
        assertThat(response.getBody()).isNotNull().hasSize(1);
        assertSoftly(softly -> {
            softly.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

            final var javaScriptFrameworkResponseDto = response.getBody().iterator().next();
            softly.assertThat(javaScriptFrameworkResponseDto.getVersions())
                    .allSatisfy(version -> assertThat(version.getId()).isNotNull());
            softly.assertThat(javaScriptFrameworkResponseDto)
                    .usingRecursiveComparison()
                    .ignoringFields("versions.id")
                    .isEqualTo(JavaScriptFrameworkObjectMother.javaScriptFramework());
        });
    }

    @Test
    @Order(3)
    void whenPatchFramework_thenReturnUpdatedFramework() {
        // given
        final var requestBody = JavaScriptFrameworkObjectMother.patchJavaScriptFrameworkRequestDto();
        assumeThat(requestBody).hasNoNullFieldsOrProperties();

        // when
        final var response = restTemplate.exchange(
                RequestEntity.patch(resolveUrl(FRAMEWORKS_BY_ID_RESOURCE), JavaScriptFrameworkObjectMother.ID)
                        .body(requestBody),
                JavaScriptFrameworkResponseDto.class
        );

        // then
        assertThat(response.getBody()).isNotNull();
        assertSoftly(softly -> {
            softly.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

            final var javaScriptFrameworkResponseDto = response.getBody();
            softly.assertThat(javaScriptFrameworkResponseDto.getId()).isEqualTo(JavaScriptFrameworkObjectMother.ID);
            softly.assertThat(javaScriptFrameworkResponseDto.getVersions()).isNotNull();
            softly.assertThat(javaScriptFrameworkResponseDto.getName()).isEqualTo(requestBody.getName());
            softly.assertThat(javaScriptFrameworkResponseDto.getHypeLevel()).isEqualTo(requestBody.getHypeLevel());
        });
    }

    @Test
    @Order(4)
    void whenCreateFrameworkVersion_thenReturnCreatedFrameworkVersion() {
        // given
        final var requestBody = JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkVersionRequestDto();
        assumeThat(requestBody).hasNoNullFieldsOrProperties();

        // when
        final var response = restTemplate.exchange(
                RequestEntity.post(resolveUrl(FRAMEWORKS_VERSIONS_RESOURCE), JavaScriptFrameworkObjectMother.ID)
                        .body(requestBody),
                JavaScriptFrameworkVersionResponseDto.class
        );

        // then
        assertThat(response.getBody()).isNotNull();
        assertSoftly(softly -> {
            softly.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);

            final var javaScriptFrameworkVersionResponseDto = response.getBody();
            softly.assertThat(javaScriptFrameworkVersionResponseDto.getId()).isNotNull();
            softly.assertThat(javaScriptFrameworkVersionResponseDto.getVersion()).isEqualTo(requestBody.getVersion());
            softly.assertThat(javaScriptFrameworkVersionResponseDto.getDeprecationDate())
                    .isEqualTo(requestBody.getDeprecationDate());
        });
    }

    @Test
    @Order(5)
    void whenUpdateFrameworkVersion_thenReturnUpdatedFrameworkVersion() {
        // given
        final var requestBody = JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkVersionRequestDto();
        requestBody.setDeprecationDate(LocalDate.of(2142, 1, 1));
        assumeThat(requestBody).hasNoNullFieldsOrProperties();

        final var frameworkId = JavaScriptFrameworkObjectMother.ID;
        final var frameworkVersionId = frameworkId + 1; // framework and framework version have common ID sequence

        // when
        final var response = restTemplate.exchange(
                RequestEntity.put(resolveUrl(FRAMEWORKS_VERSIONS_BY_ID_RESOURCE), frameworkId, frameworkVersionId)
                        .body(requestBody),
                JavaScriptFrameworkVersionResponseDto.class
        );

        // then
        assertThat(response.getBody()).isNotNull();
        assertSoftly(softly -> {
            softly.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

            final var javaScriptFrameworkVersionResponseDto = response.getBody();
            softly.assertThat(javaScriptFrameworkVersionResponseDto.getId()).isEqualTo(frameworkVersionId);
            softly.assertThat(javaScriptFrameworkVersionResponseDto.getVersion()).isEqualTo(requestBody.getVersion());
            softly.assertThat(javaScriptFrameworkVersionResponseDto.getDeprecationDate())
                    .isEqualTo(requestBody.getDeprecationDate());
        });
    }

    @Test
    @Order(6)
    void givenFrameworkWasUpdated_whenFindFramework_thenReturnUpdatedFramework() {
        // when
        final var response = restTemplate.exchange(
                RequestEntity.get(resolveUrl(FRAMEWORKS_RESOURCE)).build(),
                new ParameterizedTypeReference<Iterable<JavaScriptFrameworkResponseDto>>() {}
        );

        // then
        assertThat(response.getBody()).isNotNull().hasSize(1);
        assertSoftly(softly -> {
            softly.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

            final var javaScriptFrameworkResponseDto = response.getBody().iterator().next();
            softly.assertThat(javaScriptFrameworkResponseDto)
                    .usingRecursiveComparison()
                    .ignoringExpectedNullFields()
                    .isEqualTo(JavaScriptFrameworkObjectMother.patchJavaScriptFrameworkRequestDto());
            softly.assertThat(javaScriptFrameworkResponseDto.getVersions())
                    .hasSize(2)
                    .anySatisfy(javaScriptFrameworkVersionResponseDto -> {
                        assertThat(javaScriptFrameworkVersionResponseDto.getId()).isNotNull();
                        assertThat(javaScriptFrameworkVersionResponseDto)
                                .usingRecursiveComparison()
                                .ignoringFields("id")
                                .isEqualTo(
                                        JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkVersionRequestDto()
                                                .setDeprecationDate(LocalDate.of(2142, 1, 1))
                                );
                    })
                    .anySatisfy(javaScriptFrameworkVersionResponseDto -> {
                        assertThat(javaScriptFrameworkVersionResponseDto.getId()).isNotNull();
                        assertThat(javaScriptFrameworkVersionResponseDto)
                                .usingRecursiveComparison()
                                .ignoringFields("id")
                                .isEqualTo(JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkVersionRequestDto());
                    });
        });
    }

    @Test
    @Order(7)
    void whenDeleteFrameworkVersion_thenDeleteFrameworkVersion() {
        // given
        final var frameworkId = JavaScriptFrameworkObjectMother.ID;
        final var frameworkVersionId = frameworkId + 1; // framework and framework version have common ID sequence

        // when
        final var response = restTemplate.exchange(
                RequestEntity.delete(resolveUrl(FRAMEWORKS_VERSIONS_BY_ID_RESOURCE), frameworkId, frameworkVersionId)
                        .build(),
                Void.class
        );

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(8)
    void givenVersionWasDeleted_whenFindFramework_thenReturnFrameworkWithoutDeletedVersion() {
        // when
        final var response = restTemplate.exchange(
                RequestEntity.get(resolveUrl(FRAMEWORKS_RESOURCE)).build(),
                new ParameterizedTypeReference<Iterable<JavaScriptFrameworkResponseDto>>() {}
        );

        // then
        assertThat(response.getBody()).isNotNull().hasSize(1);
        assertSoftly(softly -> {
            softly.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

            final var javaScriptFrameworkResponseDto = response.getBody().iterator().next();
            softly.assertThat(javaScriptFrameworkResponseDto)
                    .usingRecursiveComparison()
                    .ignoringExpectedNullFields()
                    .isEqualTo(JavaScriptFrameworkObjectMother.patchJavaScriptFrameworkRequestDto());
            softly.assertThat(javaScriptFrameworkResponseDto.getVersions())
                    .singleElement()
                    .satisfies(javaScriptFrameworkVersionResponseDto -> {
                        assertThat(javaScriptFrameworkVersionResponseDto.getId()).isNotNull();
                        assertThat(javaScriptFrameworkVersionResponseDto)
                                .usingRecursiveComparison()
                                .ignoringFields("id")
                                .isEqualTo(JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkVersionRequestDto());
                    });
        });
    }

    @Test
    @Order(9)
    void whenDeleteFramework_thenDeleteFramework() {
        // when
        final var response = restTemplate.exchange(
                RequestEntity.delete(resolveUrl(FRAMEWORKS_BY_ID_RESOURCE), JavaScriptFrameworkObjectMother.ID).build(),
                Void.class
        );

        // then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @Order(10)
    void givenFrameworkWasDeleted_whenFindFramework_thenReturnEmptyArray() {
        // when
        final var response = restTemplate.exchange(
                RequestEntity.get(resolveUrl(FRAMEWORKS_RESOURCE)).build(),
                new ParameterizedTypeReference<Iterable<JavaScriptFrameworkResponseDto>>() {}
        );

        // then
        assertSoftly(softly -> {
            softly.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
            softly.assertThat(response.getBody()).isNotNull().isEmpty();
        });
    }

    private static Stream<Arguments> findFrameworksQueryParametersForPresentResource() {
        return Stream.of(
                Arguments.of("?name=" + JavaScriptFrameworkObjectMother.NAME),
                Arguments.of("?hypeLevel=" + JavaScriptFrameworkObjectMother.HYPE_LEVEL),
                Arguments.of(
                        "?name=" + JavaScriptFrameworkObjectMother.NAME
                                + "&hypeLevel=" + JavaScriptFrameworkObjectMother.HYPE_LEVEL
                )
        );
    }

    /**
     * Returns URL for given resource.
     *
     * @param resource resource path
     * @return URL for given resource
     */
    private String resolveUrl(final String resource) {
        return HOST + ":" + port + resource;
    }
}
