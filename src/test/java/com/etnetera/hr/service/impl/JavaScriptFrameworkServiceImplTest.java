package com.etnetera.hr.service.impl;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.objectmother.JavaScriptFrameworkObjectMother;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.etnetera.hr.repository.JavaScriptFrameworkVersionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assumptions.assumeThat;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link JavaScriptFrameworkServiceImpl}.
 *
 * @author Adam Bucher
 */
@ExtendWith(MockitoExtension.class)
class JavaScriptFrameworkServiceImplTest {

    @Mock
    private JavaScriptFrameworkRepository frameworkRepository;

    @Mock
    private JavaScriptFrameworkVersionRepository frameworkVersionRepository;

    @InjectMocks
    private JavaScriptFrameworkServiceImpl service;

    @Test
    void whenFindAllJavaScriptFrameworks_thenReturnAllRecordsFromRepository() {
        // given
        final var javaScriptFrameworks = List.of(JavaScriptFrameworkObjectMother.javaScriptFramework());
        when(frameworkRepository.findAll()).thenReturn(javaScriptFrameworks);

        // when
        final var result = service.findAllJavaScriptFrameworks();

        // then
        assertThat(result).isEqualTo(javaScriptFrameworks);
    }

    @Test
    void whenFindJavaScriptFramework_thenReturnFoundRecord() {
        // given
        final var javaScriptFramework = JavaScriptFrameworkObjectMother.javaScriptFramework();
        when(frameworkRepository.findById(JavaScriptFrameworkObjectMother.ID))
                .thenReturn(Optional.of(javaScriptFramework));

        // when
        final var result = service.findJavaScriptFramework(JavaScriptFrameworkObjectMother.ID);

        // then
        assertThat(result).contains(javaScriptFramework);
    }

    @Test
    void whenFindJavaScriptFrameworks_thenReturnRecordsFoundWithSpecification() {
        // given
        final var javaScriptFramework = JavaScriptFrameworkObjectMother.javaScriptFramework();
        when(frameworkRepository.findAll(notNull())).thenReturn(Set.of(javaScriptFramework));

        // when
        final var result = service.findJavaScriptFrameworks(
                JavaScriptFrameworkObjectMother.NAME, JavaScriptFrameworkObjectMother.HYPE_LEVEL
        );

        // then
        assertThat(result).containsOnly(javaScriptFramework);
    }

    @Test
    void whenCreateJavaScriptFramework_thenSaveAndReturnSavedRecord() {
        // given
        final var javaScriptFrameworkToSave = JavaScriptFrameworkObjectMother.javaScriptFramework().setId(null);
        javaScriptFrameworkToSave.getVersions().forEach(version -> version.setId(null));
        final var savedJavaScriptFramework = JavaScriptFrameworkObjectMother.javaScriptFramework();
        when(frameworkRepository.save(javaScriptFrameworkToSave)).thenReturn(savedJavaScriptFramework);

        // when
        final var result = service.createJavaScriptFramework(javaScriptFrameworkToSave);

        // then
        assertThat(result).isEqualTo(savedJavaScriptFramework);
    }

    @ParameterizedTest
    @MethodSource("javaScriptFrameworkContainingId")
    void givenEntityWithId_whenCreateJavaScriptFramework_thenThrowIllegalArgumentException(
            final JavaScriptFramework javaScriptFrameworkToSave
    ) {
        // when, then
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> service.createJavaScriptFramework(javaScriptFrameworkToSave));
        verifyNoInteractions(frameworkRepository);
    }

    @Test
    void whenUpdateJavaScriptFramework_thenSaveAndReturnUpdatedRecord() {
        // given
        final var javaScriptFrameworkToUpdate = JavaScriptFrameworkObjectMother.javaScriptFramework();
        final var savedJavaScriptFramework = JavaScriptFrameworkObjectMother.javaScriptFramework();
        when(frameworkRepository.save(javaScriptFrameworkToUpdate)).thenReturn(savedJavaScriptFramework);

        // when
        final var result = service.updateJavaScriptFramework(javaScriptFrameworkToUpdate);

        // then
        assertThat(result).isEqualTo(savedJavaScriptFramework);
    }

    @Test
    void givenEntityWithoutId_whenUpdateJavaScriptFramework_thenThrowIllegalArgumentException() {
        // given
        final var javaScriptFrameworkToUpdate = JavaScriptFrameworkObjectMother.javaScriptFramework().setId(null);

        // when, then
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> service.updateJavaScriptFramework(javaScriptFrameworkToUpdate));
        verifyNoInteractions(frameworkRepository);
    }

    @Test
    void whenDeleteJavaScriptFramework_thenDeleteRecord() {
        // when
        service.deleteJavaScriptFramework(JavaScriptFrameworkObjectMother.ID);

        // then
        verify(frameworkRepository).deleteById(JavaScriptFrameworkObjectMother.ID);
    }

    @Test
    void whenCreateJavaScriptFrameworkVersion_thenSaveAndReturnSavedRecord() {
        // given
        final var javaScriptFrameworkVersionToSave = JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion();
        javaScriptFrameworkVersionToSave.setId(null);
        final var savedJavaScriptFrameworkVersion = JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion();
        when(frameworkVersionRepository.save(javaScriptFrameworkVersionToSave))
                .thenReturn(savedJavaScriptFrameworkVersion);

        // when
        final var result = service.createJavaScriptFrameworkVersion(javaScriptFrameworkVersionToSave);

        // then
        assertThat(result).isEqualTo(savedJavaScriptFrameworkVersion);
    }

    @Test
    void givenEntityWithId_whenCreateJavaScriptFrameworkVersion_thenThrowIllegalArgumentException() {
        // given
        final var javaScriptFrameworkVersionToSave = JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion();
        assumeThat(javaScriptFrameworkVersionToSave.getId()).isNotNull();

        // when, then
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> service.createJavaScriptFrameworkVersion(javaScriptFrameworkVersionToSave));
        verifyNoInteractions(frameworkVersionRepository);
    }

    @Test
    void whenFindJavaScriptFrameworkVersion_thenReturnFoundRecord() {
        // given
        final var javaScriptFrameworkVersion = JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion();
        when(frameworkVersionRepository.findById(JavaScriptFrameworkObjectMother.ID))
                .thenReturn(Optional.of(javaScriptFrameworkVersion));

        // when
        final var result = service.findJavaScriptFrameworkVersion(JavaScriptFrameworkObjectMother.ID);

        // then
        assertThat(result).contains(javaScriptFrameworkVersion);
    }

    @Test
    void whenUpdateJavaScriptFrameworkVersion_thenSaveAndReturnSavedRecord() {
        // given
        final var javaScriptFrameworkVersionToUpdate = JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion();
        final var savedJavaScriptFrameworkVersion = JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion();
        when(frameworkVersionRepository.save(javaScriptFrameworkVersionToUpdate))
                .thenReturn(savedJavaScriptFrameworkVersion);

        // when
        final var result = service.updateJavaScriptFrameworkVersion(javaScriptFrameworkVersionToUpdate);

        // then
        assertThat(result).isEqualTo(savedJavaScriptFrameworkVersion);
    }

    @Test
    void givenEntityWithoutId_whenUpdateJavaScriptFrameworkVersion_thenThrowIllegalArgumentException() {
        // given
        final var javaScriptFrameworkVersionToUpdate = JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion();
        javaScriptFrameworkVersionToUpdate.setId(null);

        // when, then
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> service.updateJavaScriptFrameworkVersion(javaScriptFrameworkVersionToUpdate));
        verifyNoInteractions(frameworkVersionRepository);
    }

    @Test
    void whenDeleteJavaScriptFrameworkVersion_thenDeleteRecord() {
        // when
        service.deleteJavaScriptFrameworkVersion(JavaScriptFrameworkObjectMother.ID);

        // then
        verify(frameworkVersionRepository).deleteById(JavaScriptFrameworkObjectMother.ID);
    }

    private static Stream<Arguments> javaScriptFrameworkContainingId() {
        return Stream.of(
                Arguments.of(
                        JavaScriptFrameworkObjectMother.javaScriptFramework()
                                .setVersions(Set.of(
                                        JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion().setId(null)
                                ))
                ),
                Arguments.of(
                        JavaScriptFrameworkObjectMother.javaScriptFramework().setId(null)
                ),
                Arguments.of(
                        JavaScriptFrameworkObjectMother.javaScriptFramework()
                                .setId(1L)
                                .setVersions(Set.of(
                                        JavaScriptFrameworkObjectMother.javaScriptFrameworkVersion().setId(2L)
                                ))
                )
        );
    }
}
