package com.etnetera.hr.repository;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.JavaScriptFrameworkVersion;
import com.etnetera.hr.objectmother.JavaScriptFrameworkObjectMother;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.domain.Specification;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;

/**
 * Unit tests for {@link JavaScriptFrameworkRepository}.
 *
 * @author Adam Bucher
 */
@DataJpaTest
class JavaScriptFrameworkRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private JavaScriptFrameworkRepository repository;

    @Test
    void givenEntityWithVersions_whenDeleteById_thenRemoveRecordIncludingVersions() {
        // given
        final var javaScriptFrameworkToPersist = JavaScriptFrameworkObjectMother.javaScriptFramework();
        assumeThat(javaScriptFrameworkToPersist.getVersions()).hasSize(1);
        javaScriptFrameworkToPersist.setId(null);
        javaScriptFrameworkToPersist.getVersions()
                .forEach(version -> version.setId(null).setFramework(javaScriptFrameworkToPersist));
        final var persistedJavaScriptFramework = entityManager.persistAndFlush(javaScriptFrameworkToPersist);
        final var persistedJavaScriptFrameworkVersionId =
                persistedJavaScriptFramework.getVersions().iterator().next().getId();
        assumeThat(entityManager.find(JavaScriptFrameworkVersion.class, persistedJavaScriptFrameworkVersionId))
                .isNotNull();

        // when
        repository.deleteById(persistedJavaScriptFramework.getId());

        // then
        assertThat(repository.existsById(persistedJavaScriptFramework.getId())).isFalse();
        assertThat(entityManager.find(JavaScriptFrameworkVersion.class, persistedJavaScriptFrameworkVersionId))
                .isNull();
    }

    @Test
    void givenSpecification_whenFindAll_thenReturnRecordsSatisfyingSpecification() {
        // given
        final var angulular = JavaScriptFrameworkObjectMother.javaScriptFramework();
        angulular.setId(null).setVersions(null).setName("Angulular");
        entityManager.persist(angulular);
        final var rereact = JavaScriptFrameworkObjectMother.javaScriptFramework();
        rereact.setId(null).setVersions(null).setName("Rereact");
        entityManager.persist(rereact);
        final Specification<JavaScriptFramework> angulularSpecification = Specification.where(
                (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("name"), angulular.getName())
        );

        // when
        final var result = repository.findAll(angulularSpecification);

        // then
        assertThat(result).singleElement().matches(framework -> framework.getName().equals(angulular.getName()));
    }
}
