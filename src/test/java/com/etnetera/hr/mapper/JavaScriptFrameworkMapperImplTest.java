package com.etnetera.hr.mapper;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.JavaScriptFrameworkVersion;
import com.etnetera.hr.objectmother.JavaScriptFrameworkObjectMother;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

/**
 * Unit tests for {@link JavaScriptFrameworkMapperImpl}.
 *
 * @author Adam Bucher
 */
class JavaScriptFrameworkMapperImplTest {

    private final JavaScriptFrameworkMapperImpl mapper = new JavaScriptFrameworkMapperImpl();

    @Test
    void whenMapToJavaScriptFrameworkResponseDto_thenReturnMappedJavaScriptFramework() {
        // given
        final var input = JavaScriptFrameworkObjectMother.javaScriptFramework();
        final var expectedResult = JavaScriptFrameworkObjectMother.javaScriptFrameworkResponseDto();

        // when
        final var result = mapper.mapToJavaScriptFrameworkResponseDto(input);

        // then
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void whenMapToJavaScriptFrameworkResponseDtos_thenReturnMappedJavaScriptFrameworks() {
        // given
        final var input = List.of(JavaScriptFrameworkObjectMother.javaScriptFramework());
        final var expectedResult = List.of(JavaScriptFrameworkObjectMother.javaScriptFrameworkResponseDto());

        // when
        final var result = mapper.mapToJavaScriptFrameworkResponseDtos(input);

        // then
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void givenFullJavaScriptFrameworkRequestDto_whenMapToJavaScriptFramework_thenReturnMappedJavaScriptFramework() {
        // given
        final var input = JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkRequestDto();

        // when
        final var result = mapper.mapToJavaScriptFramework(input);

        // then
        assertThat(result).usingRecursiveComparison().ignoringExpectedNullFields().isEqualTo(input);
    }

    @Test
    void givenPatchJavaScriptFrameworkRequestDto_whenMapToJavaScriptFramework_thenReturnMappedJavaScriptFramework() {
        // given
        final var input = JavaScriptFrameworkObjectMother.patchJavaScriptFrameworkRequestDto();

        // when
        final var result = mapper.mapToJavaScriptFramework(input);

        // then
        assertThat(result).usingRecursiveComparison().ignoringExpectedNullFields().isEqualTo(input);
    }

    @Test
    void givenPartiallyInitializedSource_whenDecorateJavaScriptFramework_thenReturnPartiallyUpdatedTarget() {
        // given
        final var target = JavaScriptFrameworkObjectMother.javaScriptFramework();
        final var source = new JavaScriptFramework()
                .setName("New name")
                .setVersions(Set.of(new JavaScriptFrameworkVersion().setVersion("New version")));

        // when
        mapper.decorateJavaScriptFramework(target, source);

        // then
        assertSoftly(softly -> {
            final var original = JavaScriptFrameworkObjectMother.javaScriptFramework();
            softly.assertThat(target.getId()).isEqualTo(original.getId());
            softly.assertThat(target.getHypeLevel()).isEqualTo(original.getHypeLevel());

            softly.assertThat(target.getName()).isEqualTo(source.getName());
            softly.assertThat(target.getVersions()).isEqualTo(source.getVersions());
        });
    }

    @Test
    void givenFullJavaScriptFrameworkVersionRequestDto_whenMapToJavaScriptFrameworkVersion_thenReturnMappedJavaScriptFrameworkVersion() {
        // given
        final var input = JavaScriptFrameworkObjectMother.fullJavaScriptFrameworkVersionRequestDto();

        // when
        final var result = mapper.mapToJavaScriptFrameworkVersion(input);

        // then
        assertThat(result).usingRecursiveComparison().ignoringExpectedNullFields().isEqualTo(input);
    }
}
