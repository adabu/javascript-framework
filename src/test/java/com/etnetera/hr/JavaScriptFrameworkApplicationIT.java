package com.etnetera.hr;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Tests start-up of the JavaScript framework application.
 * 
 * @author Adam Bucher
 */
@SpringBootTest
class JavaScriptFrameworkApplicationIT {

    @Autowired
    private ApplicationContext applicationContext;

    @Test
    void whenStartingApplication_thenApplicationContextIsLoaded() {
        assertThat(applicationContext).isNotNull();
    }

}
