package com.etnetera.hr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Boot application class.
 * 
 * @author Etnetera
 *
 */
@SpringBootApplication
public class JavaScriptFrameworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaScriptFrameworkApplication.class);
	}

}
