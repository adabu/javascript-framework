package com.etnetera.hr.service;

import com.etnetera.hr.data.HypeLevel;
import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.JavaScriptFrameworkVersion;
import org.springframework.lang.Nullable;

import java.util.Optional;

/**
 * Provides methods for management of JavaScript frameworks.
 *
 * @author Adam Bucher
 */
public interface JavaScriptFrameworkService {

    /**
     * Returns all registered JavaScript frameworks.
     *
     * @return all JavaScript frameworks
     */
    Iterable<JavaScriptFramework> findAllJavaScriptFrameworks();

    /**
     * Returns JavaScript framework specified with {@code id}, if found.
     *
     * @param id ID of JavaScript framework to return
     * @return JavaScript framework, if found
     */
    Optional<JavaScriptFramework> findJavaScriptFramework(long id);

    /**
     * Returns found JavaScript frameworks specified searched by {@code name} and {@code hypeLevel}.
     * Arguments passed as {@code null} are not used for the search (only non-null parameters are used in filtering).
     *
     * @param name      name of JavaScript frameworks to return
     * @param hypeLevel hype level of JavaScript frameworks to return
     * @return found JavaScript frameworks
     */
    Iterable<JavaScriptFramework> findJavaScriptFrameworks(@Nullable String name, @Nullable HypeLevel hypeLevel);

    /**
     * Creates new JavaScript framework and returns it. The returned instance may differ from the input one.
     *
     * @param javaScriptFramework JavaScript framework to create
     * @return created JavaScript framework
     */
    JavaScriptFramework createJavaScriptFramework(JavaScriptFramework javaScriptFramework);

    /**
     * Updates existing JavaScript framework and returns it. The returned instance may differ from the input one.
     *
     * @param javaScriptFramework JavaScript framework to update
     * @return updated JavaScript framework
     */
    JavaScriptFramework updateJavaScriptFramework(JavaScriptFramework javaScriptFramework);

    /**
     * Deletes existing JavaScript framework.
     *
     * @param id ID of JavaScript framework to delete
     */
    void deleteJavaScriptFramework(long id);

    /**
     * Creates new JavaScript framework version and returns it. The returned instance may differ from the input one.
     *
     * @param javaScriptFrameworkVersion JavaScript framework version to create
     * @return created JavaScript framework version
     */
    JavaScriptFrameworkVersion createJavaScriptFrameworkVersion(JavaScriptFrameworkVersion javaScriptFrameworkVersion);

    /**
     * Returns JavaScript framework version specified with {@code id}, if found.
     *
     * @param id ID of JavaScript framework version to return
     * @return JavaScript framework version, if found
     */
    Optional<JavaScriptFrameworkVersion> findJavaScriptFrameworkVersion(long id);

    /**
     * Updates existing JavaScript framework version and returns it.
     * The returned instance may differ from the input one.
     *
     * @param javaScriptFrameworkVersion JavaScript framework version to update
     * @return updated JavaScript framework version
     */
    JavaScriptFrameworkVersion updateJavaScriptFrameworkVersion(JavaScriptFrameworkVersion javaScriptFrameworkVersion);

    /**
     * Deletes existing JavaScript framework version.
     *
     * @param id ID of JavaScript framework version to delete
     */
    void deleteJavaScriptFrameworkVersion(long id);
}
