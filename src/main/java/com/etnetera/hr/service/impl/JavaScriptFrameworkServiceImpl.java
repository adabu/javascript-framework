package com.etnetera.hr.service.impl;

import com.etnetera.hr.data.HypeLevel;
import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.JavaScriptFrameworkVersion;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.etnetera.hr.repository.JavaScriptFrameworkVersionRepository;
import com.etnetera.hr.service.JavaScriptFrameworkService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Manages JavaScript frameworks with use of {@link JavaScriptFrameworkRepository} and
 * {@link JavaScriptFrameworkVersionRepository} as a storage.
 *
 * @author Adam Bucher
 */
@Service
@RequiredArgsConstructor
public class JavaScriptFrameworkServiceImpl implements JavaScriptFrameworkService {

    private final JavaScriptFrameworkRepository frameworkRepository;
    private final JavaScriptFrameworkVersionRepository frameworkVersionRepository;

    @Override
    @Transactional(readOnly = true)
    public Iterable<JavaScriptFramework> findAllJavaScriptFrameworks() {
        return frameworkRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<JavaScriptFramework> findJavaScriptFramework(final long id) {
        return frameworkRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<JavaScriptFramework> findJavaScriptFrameworks(final String name, final HypeLevel hypeLevel) {
        final Specification<JavaScriptFramework> specification = (root, query, criteriaBuilder) -> {
            final var predicates = new ArrayList<Predicate>();
            if (name != null) {
                predicates.add(criteriaBuilder.equal(root.get(JavaScriptFramework.Attributes.NAME), name));
            }
            if (hypeLevel != null) {
                predicates.add(criteriaBuilder.equal(root.get(JavaScriptFramework.Attributes.HYPE_LEVEL), hypeLevel));
            }
            return criteriaBuilder.and(predicates.toArray(Predicate[]::new));
        };

        return frameworkRepository.findAll(specification);
    }

    /**
     * @throws IllegalArgumentException if {@code javaScriptFramework} contains ID (in base or any of its versions)
     */
    @Override
    @Transactional
    public JavaScriptFramework createJavaScriptFramework(final JavaScriptFramework javaScriptFramework) {
        if (javaScriptFramework.getId() != null
                || javaScriptFramework.getVersions().stream().anyMatch(version -> version.getId() != null)) {
            throw new IllegalArgumentException("JavaScript framework nor its versions can have ID before creating.");
        }
        javaScriptFramework.getVersions().forEach(version -> version.setFramework(javaScriptFramework));
        return frameworkRepository.save(javaScriptFramework);
    }

    /**
     * @throws IllegalArgumentException if {@code javaScriptFramework} lacks ID
     */
    @Override
    @Transactional
    public JavaScriptFramework updateJavaScriptFramework(final JavaScriptFramework javaScriptFramework) {
        if (javaScriptFramework.getId() == null) {
            throw new IllegalArgumentException("JavaScript framework must have ID to be updated.");
        }
        return frameworkRepository.save(javaScriptFramework);
    }

    @Override
    @Transactional
    public void deleteJavaScriptFramework(final long id) {
        frameworkRepository.deleteById(id);
    }

    @Override
    @Transactional
    public JavaScriptFrameworkVersion createJavaScriptFrameworkVersion(
            final JavaScriptFrameworkVersion javaScriptFrameworkVersion
    ) {
        if (javaScriptFrameworkVersion.getId() != null) {
            throw new IllegalArgumentException("JavaScript framework version can not have ID before creating.");
        }
        return frameworkVersionRepository.save(javaScriptFrameworkVersion);
    }

    /**
     * {@inheritDoc}
     * The returned {@link JavaScriptFrameworkVersion} has the owning framework fetched.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<JavaScriptFrameworkVersion> findJavaScriptFrameworkVersion(final long id) {
        return frameworkVersionRepository.findById(id);
    }

    @Override
    @Transactional
    public JavaScriptFrameworkVersion updateJavaScriptFrameworkVersion(
            final JavaScriptFrameworkVersion javaScriptFrameworkVersion
    ) {
        if (javaScriptFrameworkVersion.getId() == null) {
            throw new IllegalArgumentException("JavaScript framework version must have ID to be updated.");
        }
        return frameworkVersionRepository.save(javaScriptFrameworkVersion);
    }

    @Override
    @Transactional
    public void deleteJavaScriptFrameworkVersion(final long id) {
        frameworkVersionRepository.deleteById(id);
    }
}
