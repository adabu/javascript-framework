package com.etnetera.hr.mapper;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.JavaScriptFrameworkVersion;
import com.etnetera.hr.dto.FullJavaScriptFrameworkRequestDto;
import com.etnetera.hr.dto.FullJavaScriptFrameworkVersionRequestDto;
import com.etnetera.hr.dto.JavaScriptFrameworkResponseDto;
import com.etnetera.hr.dto.JavaScriptFrameworkVersionResponseDto;
import com.etnetera.hr.dto.PatchJavaScriptFrameworkRequestDto;
import org.mapstruct.BeanMapping;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

/**
 * Provides mapping between representations of JavaScript framework.
 *
 * @author Adam Bucher
 */
@Mapper(componentModel = "spring", collectionMappingStrategy = CollectionMappingStrategy.TARGET_IMMUTABLE)
public interface JavaScriptFrameworkMapper {

    /**
     * Maps {@link JavaScriptFramework} to {@link JavaScriptFrameworkResponseDto}.
     *
     * @param javaScriptFramework object to map
     * @return mapping result
     */
    JavaScriptFrameworkResponseDto mapToJavaScriptFrameworkResponseDto(JavaScriptFramework javaScriptFramework);

    /**
     * Maps {@link JavaScriptFrameworkVersion} to {@link JavaScriptFrameworkVersionResponseDto}.
     *
     * @param javaScriptFrameworkVersion object to map
     * @return mapping result
     */
    JavaScriptFrameworkVersionResponseDto mapToJavaScriptFrameworkVersionResponseDto(
            JavaScriptFrameworkVersion javaScriptFrameworkVersion
    );

    /**
     * Maps {@link Iterable} of {@link JavaScriptFramework}
     * to {@link Iterable} of {@link JavaScriptFrameworkResponseDto}.
     *
     * @param javaScriptFrameworks objects to map
     * @return mapping result
     */
    Iterable<JavaScriptFrameworkResponseDto> mapToJavaScriptFrameworkResponseDtos(
            Iterable<JavaScriptFramework> javaScriptFrameworks
    );

    /**
     * Maps {@link FullJavaScriptFrameworkRequestDto} to {@link JavaScriptFramework}.
     *
     * @param fullJavaScriptFrameworkRequestDto object to map
     * @return mapping result
     */
    JavaScriptFramework mapToJavaScriptFramework(
            FullJavaScriptFrameworkRequestDto fullJavaScriptFrameworkRequestDto
    );

    /**
     * Maps {@link FullJavaScriptFrameworkVersionRequestDto} to {@link JavaScriptFrameworkVersion}.
     *
     * @param fullJavaScriptFrameworkVersionRequestDto object to map
     * @return mapping result
     */
    JavaScriptFrameworkVersion mapToJavaScriptFrameworkVersion(
            FullJavaScriptFrameworkVersionRequestDto fullJavaScriptFrameworkVersionRequestDto
    );

    /**
     * Maps {@link PatchJavaScriptFrameworkRequestDto} to {@link JavaScriptFramework}.
     *
     * @param patchJavaScriptFrameworkRequestDto object to map
     * @return mapping result
     */
    JavaScriptFramework mapToJavaScriptFramework(
            PatchJavaScriptFrameworkRequestDto patchJavaScriptFrameworkRequestDto
    );

    /**
     * Maps {@link PatchJavaScriptFrameworkRequestDto} to {@link JavaScriptFramework}.
     *
     * @param target object to be decorated
     * @param source decoration source, only non-null properties of this object will be set to {@code target}
     */
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void decorateJavaScriptFramework(@MappingTarget JavaScriptFramework target, JavaScriptFramework source);
}
