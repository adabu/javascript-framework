package com.etnetera.hr.data;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

/**
 * Simple data entity describing basic properties of every JavaScript framework.
 * 
 * @author Etnetera
 *
 */
@Entity
@Data
public class JavaScriptFramework {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = Attributes.NAME, nullable = false, length = 30)
	private String name;

	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, mappedBy = "framework")
	private Set<JavaScriptFrameworkVersion> versions;

	@Column(name = Attributes.HYPE_LEVEL, nullable = false)
	@Enumerated(EnumType.STRING)
	private HypeLevel hypeLevel;

	public JavaScriptFramework() {
	}

	public JavaScriptFramework(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "JavaScriptFramework [id=" + id + ", name=" + name + ", versions=" + versions + "]";
	}

	/**
	 * Contains names of {@link JavaScriptFramework} attributes.
	 */
	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static class Attributes {

		public static final String NAME = "name";
		public static final String HYPE_LEVEL = "hypeLevel";
	}
}
