package com.etnetera.hr.data;

import com.fasterxml.jackson.annotation.JsonAlias;

/**
 * Defines level of hype.
 */
public enum HypeLevel {

    /**
     * No hype.
     */
    NONE,

    /**
     * Few people talk about it.
     */
    LOW,

    /**
     * Influencers mention it.
     */
    MIDDLE,

    /**
     * It is a common conversation topic.
     */
    HIGH,

    /**
     * People hardly talk about anything else.
     */
    @JsonAlias("EVEN_MY_GRANDMA_TALKS_ABOUT_IT")
    MAX
}
