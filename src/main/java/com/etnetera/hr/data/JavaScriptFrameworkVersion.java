package com.etnetera.hr.data;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

/**
 * Data of specific version of a JavaScript framework.
 *
 * @author Adam Bucher
 * @see JavaScriptFramework
 */
@Entity
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode(exclude = "framework")
@ToString(exclude = "framework")
public class JavaScriptFrameworkVersion {

    @Id
    @GeneratedValue
    Long id;

    @Column(nullable = false, length = 30)
    String version;

    @Column(name = "deprecation_date")
    LocalDate deprecationDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "framework_id", nullable = false, updatable = false)
    JavaScriptFramework framework;
}
