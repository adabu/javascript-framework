package com.etnetera.hr.dto;

import com.etnetera.hr.data.HypeLevel;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Set;

/**
 * DTO representation of JavaScript framework for use as response.
 *
 * @author Adam Bucher
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JavaScriptFrameworkResponseDto {

	Long id;
	String name;
	Set<JavaScriptFrameworkVersionResponseDto> versions;
	HypeLevel hypeLevel;
}
