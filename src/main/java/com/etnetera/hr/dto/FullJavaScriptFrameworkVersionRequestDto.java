package com.etnetera.hr.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * DTO representation of JavaScript framework version for use as request with full resource, such as create or update.
 *
 * @author Adam Bucher
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FullJavaScriptFrameworkVersionRequestDto {

    @NotBlank
    @Size(max = 30)
    String version;

    LocalDate deprecationDate;
}
