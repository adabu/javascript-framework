package com.etnetera.hr.dto;

import com.etnetera.hr.data.HypeLevel;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * DTO representation of JavaScript framework for use as request with full resource, such as create or update.
 *
 * @author Adam Bucher
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FullJavaScriptFrameworkRequestDto {

    @NotBlank
    @Size(max = 30)
    String name;

    @Valid
    Set<FullJavaScriptFrameworkVersionRequestDto> versions;

    @NotNull
    HypeLevel hypeLevel;
}
