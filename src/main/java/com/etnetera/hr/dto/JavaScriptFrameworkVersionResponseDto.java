package com.etnetera.hr.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

/**
 * DTO representation of JavaScript framework version for use as response.
 *
 * @author Adam Bucher
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JavaScriptFrameworkVersionResponseDto {

	Long id;
	String version;
	LocalDate deprecationDate;
}
