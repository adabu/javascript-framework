package com.etnetera.hr.dto;

import com.etnetera.hr.data.HypeLevel;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * DTO representation of JavaScript framework for use as patch request.
 *
 * @author Adam Bucher
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PatchJavaScriptFrameworkRequestDto {

    String name;
    HypeLevel hypeLevel;
}
