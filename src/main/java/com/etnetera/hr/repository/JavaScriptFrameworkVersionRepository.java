package com.etnetera.hr.repository;

import com.etnetera.hr.data.JavaScriptFrameworkVersion;
import org.springframework.data.repository.CrudRepository;

/**
 * CRUD repository for {@link JavaScriptFrameworkVersion}.
 *
 * @author Adam Bucher
 */
public interface JavaScriptFrameworkVersionRepository extends CrudRepository<JavaScriptFrameworkVersion, Long> {
}
