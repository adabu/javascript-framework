package com.etnetera.hr.controller;

import com.etnetera.hr.data.HypeLevel;
import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.JavaScriptFrameworkVersion;
import com.etnetera.hr.dto.FullJavaScriptFrameworkRequestDto;
import com.etnetera.hr.dto.FullJavaScriptFrameworkVersionRequestDto;
import com.etnetera.hr.dto.JavaScriptFrameworkResponseDto;
import com.etnetera.hr.dto.JavaScriptFrameworkVersionResponseDto;
import com.etnetera.hr.dto.PatchJavaScriptFrameworkRequestDto;
import com.etnetera.hr.mapper.JavaScriptFrameworkMapper;
import com.etnetera.hr.service.JavaScriptFrameworkService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

/**
 * Simple REST controller for accessing application logic.
 * 
 * @author Etnetera
 *
 */
@RestController
@RequestMapping("/frameworks")
@RequiredArgsConstructor
public class JavaScriptFrameworkController {

    private final JavaScriptFrameworkService service;
    private final JavaScriptFrameworkMapper mapper;

    /**
     * Finds JavaScript frameworks based on query parameters. When there are none, all JavaScript frameworks
     * are returned, otherwise JavaScript frameworks with specified values are returned.
     *
     * @param name      optional name filter of JavaScript frameworks to return
     * @param hypeLevel optional hype level filter of JavaScript frameworks to return
     * @return found JavaScript frameworks
     */
    @GetMapping
    public Iterable<JavaScriptFrameworkResponseDto> findFrameworks(
            @RequestParam(required = false) final String name, @RequestParam(required = false) final HypeLevel hypeLevel
    ) {
        Iterable<JavaScriptFramework> foundJavaScriptFrameworks;
        if (name != null || hypeLevel != null) {
            foundJavaScriptFrameworks = service.findJavaScriptFrameworks(name, hypeLevel);
        } else {
            foundJavaScriptFrameworks = service.findAllJavaScriptFrameworks();
        }
        return mapper.mapToJavaScriptFrameworkResponseDtos(foundJavaScriptFrameworks);
    }

    /**
     * Finds JavaScript framework.
     *
     * @param id ID of JavaScript framework to find
     * @return found JavaScript framework
     */
    @GetMapping("/{id}")
    public JavaScriptFrameworkResponseDto findFramework(@PathVariable final long id) {
        final var foundJavaScriptFramework = getFramework(id);
        return mapper.mapToJavaScriptFrameworkResponseDto(foundJavaScriptFramework);
    }

    /**
     * Creates new JavaScript framework and returns it.
     *
     * @param request creation request
     * @return created JavaScript framework
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public JavaScriptFrameworkResponseDto createFramework(
            @RequestBody @Valid final FullJavaScriptFrameworkRequestDto request
    ) {
        final var javaScriptFrameworkToCreate = mapper.mapToJavaScriptFramework(request);
        final var createdJavaScriptFramework = service.createJavaScriptFramework(javaScriptFrameworkToCreate);
        return mapper.mapToJavaScriptFrameworkResponseDto(createdJavaScriptFramework);
    }

    /**
     * Updates existing JavaScript framework and returns it.
     *
     * @param id      ID of existing JavaScript framework
     * @param request update request
     * @return updated JavaScript framework
     */
    @PatchMapping("/{id}")
    public JavaScriptFrameworkResponseDto patchFramework(
            @PathVariable final long id, @RequestBody @Valid final PatchJavaScriptFrameworkRequestDto request
    ) {
        final var existingJavaScriptFramework = getFramework(id);
        final var javaScriptFrameworkPatchSource = mapper.mapToJavaScriptFramework(request);
        mapper.decorateJavaScriptFramework(existingJavaScriptFramework, javaScriptFrameworkPatchSource);

        final var updatedJavaScriptFramework = service.updateJavaScriptFramework(existingJavaScriptFramework);

        return mapper.mapToJavaScriptFrameworkResponseDto(updatedJavaScriptFramework);
    }

    /**
     * Deletes JavaScript framework.
     *
     * @param id ID of existing JavaScript framework
     */
    @DeleteMapping("/{id}")
    public void deleteFramework(@PathVariable final long id) {
        getFramework(id); // verify existence of framework

        service.deleteJavaScriptFramework(id);
    }

    /**
     * Creates new JavaScript framework version for existing JavaScript framework and returns it.
     *
     * @param id      ID of existing JavaScript framework
     * @param request creation request
     * @return created JavaScript framework version
     */
    @PostMapping("/{id}/versions")
    @ResponseStatus(HttpStatus.CREATED)
    public JavaScriptFrameworkVersionResponseDto createFrameworkVersion(
            @PathVariable final long id, @RequestBody @Valid final FullJavaScriptFrameworkVersionRequestDto request
    ) {
        final var existingJavaScriptFramework = getFramework(id);
        final var javaScriptFrameworkVersionToCreate = mapper.mapToJavaScriptFrameworkVersion(request);
        javaScriptFrameworkVersionToCreate.setFramework(existingJavaScriptFramework);

        final var createdJavaScriptFrameworkVersion = service
                .createJavaScriptFrameworkVersion(javaScriptFrameworkVersionToCreate);

        return mapper.mapToJavaScriptFrameworkVersionResponseDto(createdJavaScriptFrameworkVersion);
    }

    /**
     * Updates JavaScript framework version for existing JavaScript framework and returns it.
     *
     * @param frameworkId        ID of existing JavaScript framework
     * @param frameworkVersionId ID of existing JavaScript framework version
     * @param request            update request
     * @return updated JavaScript framework version
     */
    @PutMapping("/{frameworkId}/versions/{frameworkVersionId}")
    public JavaScriptFrameworkVersionResponseDto updateFrameworkVersion(
            @PathVariable final long frameworkId,
            @PathVariable final long frameworkVersionId,
            @RequestBody @Valid final FullJavaScriptFrameworkVersionRequestDto request
    ) {
        getFrameworkVersion(frameworkId, frameworkVersionId); // verify existence of framework and version
        final var javaScriptFrameworkVersionToUpdate = mapper.mapToJavaScriptFrameworkVersion(request);
        javaScriptFrameworkVersionToUpdate.setId(frameworkVersionId);

        final var updatedJavaScriptFrameworkVersion = service
                .updateJavaScriptFrameworkVersion(javaScriptFrameworkVersionToUpdate);

        return mapper.mapToJavaScriptFrameworkVersionResponseDto(updatedJavaScriptFrameworkVersion);
    }

    /**
     * Deletes JavaScript framework version for existing JavaScript framework.
     *
     * @param frameworkId        ID of existing JavaScript framework
     * @param frameworkVersionId ID of existing JavaScript framework version
     */
    @DeleteMapping("/{frameworkId}/versions/{frameworkVersionId}")
    public void deleteFrameworkVersion(
            @PathVariable final long frameworkId, @PathVariable final long frameworkVersionId
    ) {
        getFrameworkVersion(frameworkId, frameworkVersionId); // verify existence of framework and version

        service.deleteJavaScriptFrameworkVersion(frameworkVersionId);
    }

    /**
     * Returns JavaScript framework, if it finds it. Otherwise, throws exception with {@link HttpStatus#NOT_FOUND}.
     *
     * @param id ID of JavaScript framework to find
     * @return found JavaScript framework
     * @throws ResponseStatusException with {@link HttpStatus#NOT_FOUND} if JavaScript framework with {@code id}
     *                                 is not found
     */
    private JavaScriptFramework getFramework(final long id) {
        return service.findJavaScriptFramework(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    /**
     * Returns JavaScript framework, if it finds it. Otherwise, throws exception with {@link HttpStatus#NOT_FOUND}.
     *
     * @param frameworkId        id of JavaScript framework owning the version to find
     * @param frameworkVersionId id of JavaScript framework version to find
     * @return found JavaScript framework
     * @throws ResponseStatusException with {@link HttpStatus#NOT_FOUND} if JavaScript framework version with
     *                                 {@code frameworkVersionId} is not found or is not owned by JavaScript framework
     *                                 with {@code frameworkId}
     */
    private JavaScriptFrameworkVersion getFrameworkVersion(final long frameworkId, final long frameworkVersionId) {
        return service.findJavaScriptFrameworkVersion(frameworkVersionId)
                .filter(version -> version.getFramework().getId() == frameworkId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}
